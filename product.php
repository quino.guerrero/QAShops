<?php

class Product
{
    public static function stock(
        $productId,
        $quantityAvailable,
        $cache = false,
        $cacheDuration = 60,
        $securityStockConfig = null
    )
    {
        if ($quantityAvailable < 0) {
            return $quantityAvailable;
        }

        // Obtenemos el stock bloqueado por pedidos en curso
        $ordersQuantity = self::getOrdersQuantity($productId, $cache, $cacheDuration);

        // Obtenemos el stock bloqueado
        $blockedStockQuantity = self::getBlockedStockQuantity($productId, $cache, $cacheDuration);

        // Calculamos las unidades disponibles
        $quantity = $quantityAvailable - @$ordersQuantity - @$blockedStockQuantity;
        if (!empty($securityStockConfig)) {
            $quantity = ShopChannel::applySecurityStockConfig(
                $quantity,
                @$securityStockConfig->mode,
                @$securityStockConfig->quantity
            );
        }
        return $quantity > 0 ? $quantity : 0;
    }

    private static function getOrdersQuantity($productId, $cache=false, $cacheDuration=false)
    {
        if ($cache) {
            return OrderLine::getDb()->cache(
                function ($db) use ($productId) {
                    return self::getOrdersQuantity($productId);
                },
                $cacheDuration
            );
        }
        return OrderLine::find()
            ->select('SUM(quantity) as quantity')
            ->joinWith('order')
            ->where(
                "(order.status = '" . Order::STATUS_PENDING .
                "' OR order.status = '" . Order::STATUS_PROCESSING .
                "' OR order.status = '" . Order::STATUS_WAITING_ACCEPTANCE .
                "') AND order_line.product_id = $productId"
            )->scalar();
    }

    private static function getBlockedStockQuantity($productId, $cache=false, $cacheDuration=false)
    {
        if ($cache) {
            return BlockedStock::getDb()->cache(
                function ($db) use ($productId) {
                    return self::getBlockedStockQuantity($productId);
                },
                $cacheDuration
            );
        }
        return BlockedStock::find()
            ->select('SUM(quantity) as quantity')
            ->joinWith('shoppingCart')
            ->where(
                "blocked_stock.product_id = $productId AND blocked_stock_to_date > '"
                . date('Y-m-d H:i:s') . "' AND (shopping_cart_id IS NULL OR shopping_cart.status = '"
                . ShoppingCart::STATUS_PENDING . "')"
            )->scalar();
    }
}
