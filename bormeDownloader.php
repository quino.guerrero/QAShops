<?php

include 'vendor/autoload.php';

use \Smalot\PdfParser\Parser;

class BormeDownloader
{
    private static $REGEXP = '#https://www.boe.es/borme/dias/\d{4}/\d{2}/\d{2}/pdfs/((BORME-[\w-]+).pdf)#';
    private static $TEMPDIR = '';
    private static $MAX_RETRIES = 3;
    private $parser;

    public function __construct() {
        $this->parser = new Parser();
    }

    public function downloadBorme($url)
    {
        $matches = [];

        if (!preg_match(self::$REGEXP, $url, $matches))
            throw new Exception('Invalid url');

        $pdffile = self::$TEMPDIR.$matches[1];
        $txtfile = self::$TEMPDIR.$matches[2].'.txt';

        $retries = 0;

        while (($pdf = file_get_contents($url)) === false && $retries < self::$MAX_RETRIES) {
            $retries++;
        }

        if ($pdf === false)
            throw new Exception("Error reading ".$url);

        try {
            file_put_contents($pdffile, $pdf);
            $pdf = $this->parser->parseFile($pdffile);
            file_put_contents($txtfile, $pdf->getText());
            unlink($pdffile);
            return $txtfile;
        } catch (Exception $e) {
            throw $e;
        }
    }
}

